package com.android.ioasys.test;

import android.content.ComponentName;
import android.support.test.espresso.IdlingRegistry;
import android.support.test.espresso.IdlingResource;
import android.support.test.espresso.intent.rule.IntentsTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.android.ioasys.test.android.EnterpriseActivity_;
import com.android.ioasys.test.android.LoginActivity_;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.InstrumentationRegistry.getTargetContext;
import static android.support.test.espresso.Espresso.closeSoftKeyboard;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.intent.Intents.intended;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasComponent;
import static android.support.test.espresso.matcher.RootMatchers.withDecorView;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.not;

@RunWith(AndroidJUnit4.class)
public class LoginActivityTest {

    private IdlingResource idlingResource;

    @Rule
    public IntentsTestRule<LoginActivity_> activityTestRule = new IntentsTestRule<>(LoginActivity_.class);

    @Before
    public void registerIdlingResource() {
        idlingResource = activityTestRule.getActivity().getIdlingResource();
        IdlingRegistry.getInstance().register(idlingResource);
    }

    @Test
    public void clickLoginButton_WrongUser() {
        onView(withId(R.id.edit_text_email)).perform(typeText("testeapple@iosys.com.br"));
        closeSoftKeyboard();
        onView(withId(R.id.edit_text_password)).perform(typeText("12341234"));
        closeSoftKeyboard();

        onView(withId(R.id.button_login)).perform(click());

        onView(withText(R.string.login_unauthorized)).inRoot(withDecorView(not(is(activityTestRule.getActivity().getWindow().getDecorView())))).check(matches(isDisplayed()));
    }

    @Test
    public void clickLoginButton_EmailInvalid() {
        onView(withId(R.id.edit_text_email)).perform(typeText("testeappleiosys.com.br"));
        closeSoftKeyboard();
        onView(withId(R.id.edit_text_password)).perform(typeText("12341234"));
        closeSoftKeyboard();

        onView(withId(R.id.button_login)).perform(click());

        onView(withText(R.string.email_invalid)).inRoot(withDecorView(not(is(activityTestRule.getActivity().getWindow().getDecorView())))).check(matches(isDisplayed()));
    }

    @Test
    public void clickLoginButton_WithoutPassword() {
        onView(withId(R.id.edit_text_email)).perform(typeText("testeapple@iosyas.com.br"));
        closeSoftKeyboard();
        onView(withId(R.id.edit_text_password)).perform(typeText(""));
        closeSoftKeyboard();

        onView(withId(R.id.button_login)).perform(click());

        onView(withText(R.string.password_require)).inRoot(withDecorView(not(is(activityTestRule.getActivity().getWindow().getDecorView())))).check(matches(isDisplayed()));
    }

    @Test
    public void clickLoginButton_WithoutEmail() {
        onView(withId(R.id.edit_text_email)).perform(typeText(""));
        closeSoftKeyboard();
        onView(withId(R.id.edit_text_password)).perform(typeText(""));
        closeSoftKeyboard();

        onView(withId(R.id.button_login)).perform(click());

        onView(withText(R.string.email_required)).inRoot(withDecorView(not(is(activityTestRule.getActivity().getWindow().getDecorView())))).check(matches(isDisplayed()));
    }

    @Test
    public void clickLoginButton_LoginSuccess() {
        onView(withId(R.id.edit_text_email)).perform(typeText("testeapple@ioasys.com.br"));
        closeSoftKeyboard();
        onView(withId(R.id.edit_text_password)).perform(typeText("12341234"));
        closeSoftKeyboard();

        onView(withId(R.id.button_login)).perform(click());

        intended(hasComponent(new ComponentName(getTargetContext(), EnterpriseActivity_.class)));
    }


    @After
    public void unRegisterIdlingResource() {
        if (idlingResource != null)
            IdlingRegistry.getInstance().unregister(idlingResource);
    }
}
