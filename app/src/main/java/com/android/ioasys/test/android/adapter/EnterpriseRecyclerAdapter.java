package com.android.ioasys.test.android.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.ioasys.test.R;
import com.android.ioasys.test.android.EnterpriseDetailsActivity_;
import com.android.ioasys.test.model.Enterprise;
import com.android.ioasys.test.utils.ImageLoader;

import java.util.List;

import javax.inject.Inject;

public class EnterpriseRecyclerAdapter extends RecyclerView.Adapter<EnterpriseRecyclerAdapter.EnterpriseViewHolder> {

    private List<Enterprise> enterpriseList;

    @Inject
    public EnterpriseRecyclerAdapter() {
    }

    @NonNull
    @Override
    public EnterpriseViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.enterprise_item, viewGroup, false);
        return new EnterpriseViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull EnterpriseViewHolder viewHolder, int position) {
        viewHolder.onBind(enterpriseList.get(position));
    }

    @Override
    public int getItemCount() {
        if (enterpriseList != null) {
            return enterpriseList.size();
        } else {
            return 0;
        }
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public void addAll(List<Enterprise> enterpriseList) {
        this.enterpriseList = enterpriseList;
        notifyDataSetChanged();
    }

    class EnterpriseViewHolder extends RecyclerView.ViewHolder {

        private TextView enterpriseTitle, enterpriseCountry, enterpriseType, enterpriseInitial;
        private ImageView enterprisePhoto;
        private View root;

        EnterpriseViewHolder(@NonNull View itemView) {
            super(itemView);
            this.enterpriseTitle = itemView.findViewById(R.id.text_view_enterprise_title);
            this.enterpriseCountry = itemView.findViewById(R.id.text_view_enterprise_country);
            this.enterpriseType = itemView.findViewById(R.id.text_view_enterprise_type);
            this.enterprisePhoto = itemView.findViewById(R.id.image_view_enterprise_photo);
            this.enterpriseInitial = itemView.findViewById(R.id.text_view_enterprise_initial);
            this.root = itemView.findViewById(R.id.layout_root);
        }

        void onBind(Enterprise model) {
            final String enterpriseName = model.getEnterpriseName();
            final String photoUrl = model.getPhoto();
            final String enterpriseDescription = model.getDescription();

            if (enterpriseName != null) {
                enterpriseTitle.setText(enterpriseName);
                enterpriseInitial.setText(String.valueOf(enterpriseName.charAt(0)));
            }

            if (model.getCountry() != null)
                enterpriseCountry.setText(model.getCountry());

            if (model.getEnterpriseType() != null && model.getEnterpriseType().getEnterpriseTypeName() != null)
                enterpriseType.setText(model.getEnterpriseType().getEnterpriseTypeName());

            if (photoUrl != null)
                ImageLoader.loadImageFromApiUrl(photoUrl, enterprisePhoto, enterpriseInitial);

            root.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    EnterpriseDetailsActivity_.intent(itemView.getContext())
                            .enterpriseName(enterpriseName)
                            .enterprisePhotoUrl(photoUrl)
                            .enterpriseDescription(enterpriseDescription)
                            .start();
                }
            });
        }
    }
}
