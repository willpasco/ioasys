package com.android.ioasys.test.repository;

import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;
import android.util.Log;

import com.android.ioasys.test.model.AuthRequest;
import com.android.ioasys.test.model.UserResponse;
import com.android.ioasys.test.service.IoasysService;
import com.pixplicity.easyprefs.library.Prefs;

import javax.inject.Inject;

import okhttp3.Headers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserRepository {

    static final String ACCESS_TOKEN = "access-token";
    static final String UID = "uid";
    static final String CLIENT = "client";
    private static final String TAG = "UserRepository";
    private IoasysService service;
    private AuthRequest authRequest;
    private MutableLiveData<Integer> requestCodeMutableLiveData;

    @Inject
    public UserRepository(IoasysService service, AuthRequest authRequest) {
        this.service = service;
        this.authRequest = authRequest;
        this.requestCodeMutableLiveData = new MutableLiveData<>();
    }

    public MutableLiveData<Integer> doLogin(String email, String password) {
        authRequest.setEmail(email);
        authRequest.setPassword(password);
        Call<UserResponse> call = service.doLogin(authRequest);
        call.enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(@NonNull Call<UserResponse> call, @NonNull Response<UserResponse> response) {
                if (response.code() == IoasysService.REQUEST_OK) {
                    saveTokenInPreferences(response.headers());
                }
                requestCodeMutableLiveData.postValue(response.code());
            }

            @Override
            public void onFailure(@NonNull Call<UserResponse> call, @NonNull Throwable t) {
                requestCodeMutableLiveData.postValue(IoasysService.REQUEST_APP_ERROR);
                Log.e(TAG, t.getMessage());
            }
        });

        return requestCodeMutableLiveData;
    }

    private void saveTokenInPreferences(Headers headers) {
        String accessToken = headers.get(ACCESS_TOKEN);
        String client = headers.get(CLIENT);
        String uid = headers.get(UID);
        if (accessToken != null && client != null && uid != null) {
            Prefs.putString(ACCESS_TOKEN, accessToken);
            Prefs.putString(CLIENT, client);
            Prefs.putString(UID, uid);
        }
    }
}
