package com.android.ioasys.test.model;

import javax.inject.Inject;

public class AuthRequest {

    private String email;
    private String password;

    @Inject
    public AuthRequest() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
