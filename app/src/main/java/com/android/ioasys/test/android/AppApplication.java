package com.android.ioasys.test.android;

import android.app.Application;
import android.content.ContextWrapper;

import com.android.ioasys.test.di.DaggerMainComponent;
import com.android.ioasys.test.di.MainComponent;
import com.pixplicity.easyprefs.library.Prefs;

public class AppApplication extends Application {

    private static MainComponent component;

    public static MainComponent getComponent() {
        return component;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        initDagger();
        initEasyPrefs();
    }

    private void initEasyPrefs() {
        new Prefs.Builder()
                .setContext(this)
                .setMode(ContextWrapper.MODE_PRIVATE)
                .setPrefsName(getPackageName())
                .setUseDefaultSharedPreference(true)
                .build();
    }

    private void initDagger() {
        component = DaggerMainComponent.builder().build();
    }
}
