package com.android.ioasys.test.utils;

import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.ioasys.test.R;
import com.android.ioasys.test.di.NetworkModule;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

public class ImageLoader {

    public static void loadImageFromApiUrl(String url, ImageView view, final TextView initialLetter) {
        Glide.with(view.getContext())
                .load(NetworkModule.BASE_URL + url)
                .placeholder(R.drawable.glide_placeholder)
                .addListener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        initialLetter.setVisibility(View.GONE);
                        return false;
                    }
                })
                .into(view);
    }

    public static void loadImageFromResource(int res, ImageView view) {
        Glide.with(view.getContext())
                .load(res)
                .into(view);
    }
}
