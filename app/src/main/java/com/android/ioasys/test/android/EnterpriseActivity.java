package com.android.ioasys.test.android;

import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.transition.TransitionManager;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.ioasys.test.R;
import com.android.ioasys.test.android.adapter.EnterpriseRecyclerAdapter;
import com.android.ioasys.test.di.ViewModelFactory;
import com.android.ioasys.test.model.Enterprise;
import com.android.ioasys.test.service.DataWrapper;
import com.android.ioasys.test.viewmodel.EnterpriseViewModel;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.List;

import javax.inject.Inject;

@SuppressLint("Registered")
@EActivity(R.layout.activity_enterprise)
public class EnterpriseActivity extends AppCompatActivity {

    @ViewById(R.id.recycler_view)
    RecyclerView recyclerView;

    @ViewById(R.id.image_view_search)
    ImageView searchIcon;

    @ViewById(R.id.edit_text_search)
    EditText editSearch;

    @ViewById(R.id.image_view_toolbar_logo)
    ImageView toolbarLogo;

    @ViewById(R.id.layout_constraint_toolbar)
    ConstraintLayout toolbarConstraintLayout;

    @ViewById(R.id.text_view_no_content)
    TextView textNoContent;

    @ViewById(R.id.progress_bar_loading)
    ProgressBar progressBar;

    @ViewById(R.id.image_view_clear)
    ImageView clearIcon;

    @Inject
    ViewModelFactory viewModelFactory;

    @Inject
    EnterpriseRecyclerAdapter adapter;
    private boolean isTyping = false;
    private boolean isClearDrawable;

    @AfterViews
    void afterViews() {

        AppApplication.getComponent().inject(this);
        final EnterpriseViewModel viewModel = ViewModelProviders.of(this, viewModelFactory).get(EnterpriseViewModel.class);

        configEditSearchField(viewModel);
        configSearchIcon();
        configCollapseAction();

        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        recyclerView.setAdapter(adapter);

        viewModel.getDataWrapperMutableLiveData().observe(EnterpriseActivity.this, new Observer<DataWrapper<List<Enterprise>>>() {
            @Override
            public void onChanged(@Nullable DataWrapper<List<Enterprise>> dataWrapper) {
                if (dataWrapper != null && dataWrapper.getData() != null) {
                    if (dataWrapper.getData().size() > 0) {
                        showContentState();
                        adapter.addAll(dataWrapper.getData());
                    } else {
                        textNoContent.setText(getResources().getString(R.string.enterprise_not_found));
                        showNoContentState();
                    }
                } else if (dataWrapper != null) {
                    textNoContent.setText(getResources().getString(R.string.enterprise_request_error));
                    showNoContentState();
                }
            }
        });


    }


    private void configSearchIcon() {

        searchIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toolbarLogo.setVisibility(View.GONE);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    TransitionManager.beginDelayedTransition(toolbarConstraintLayout);
                    changeSearchIconConstraints();
                } else {
                    startSearchIconAnimation(R.anim.left_to_right);
                }

            }
        });
    }

    private void configEditSearchField(final EnterpriseViewModel viewModel) {

        editSearch.setTextColor(getResources().getColor(android.R.color.white));
        editSearch.getBackground().mutate().setColorFilter(getResources().getColor(android.R.color.white), PorterDuff.Mode.SRC_ATOP);

        editSearch.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    hideSoftKeyboard();
                    showLoadingState();
                    viewModel.fetchData(editSearch.getText().toString());
                    return true;
                }
                return false;
            }
        });

        editSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence text, int start, int before, int count) {
                isTyping = text.length() > 0;
                configIconAction();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void configIconAction() {
        if (isTyping) {

            if (!isClearDrawable) {
                Drawable drawable = getResources().getDrawable(R.drawable.ic_clear);
                startRotateAnimation(drawable, 0f, 360f);
                isClearDrawable = true;
            }

            configClearAction();

        } else {
            Drawable drawable = getResources().getDrawable(R.drawable.ic_collapse);
            startRotateAnimation(drawable, 360f, 0f);
            isClearDrawable = false;

            configCollapseAction();
        }
    }

    private void configClearAction() {
        clearIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editSearch.setText("");
            }
        });
    }

    private void configCollapseAction() {
        clearIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideSoftKeyboard();

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    TransitionManager.beginDelayedTransition(toolbarConstraintLayout);
                    resetSearchIconConstraints();
                } else {
                    startSearchIconAnimation(R.anim.right_to_left);
                }
            }
        });
    }

    private void hideSoftKeyboard() {
        InputMethodManager imm = (InputMethodManager) this.getSystemService(Activity.INPUT_METHOD_SERVICE);
        View view = getCurrentFocus();
        if (view == null) {
            view = new View(this);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

    }

    private void showContentState() {
        textNoContent.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);
    }

    public void showLoadingState() {
        textNoContent.setVisibility(View.GONE);
        recyclerView.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
    }

    private void showNoContentState() {
        textNoContent.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.GONE);
        progressBar.setVisibility(View.GONE);
    }

    private void startSearchIconAnimation(int resource) {
        Animation slideLeftToRightAnimation = AnimationUtils.loadAnimation(EnterpriseActivity.this, resource);
        slideLeftToRightAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                changeSearchIconConstraints();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        searchIcon.startAnimation(slideLeftToRightAnimation);

    }

    private void changeSearchIconConstraints() {
        ConstraintSet constraintSet = new ConstraintSet();
        constraintSet.clone(toolbarConstraintLayout);
        constraintSet.connect(R.id.image_view_search, ConstraintSet.START, ConstraintSet.PARENT_ID, ConstraintSet.START, 0);
        constraintSet.clear(R.id.image_view_search, ConstraintSet.END);
        constraintSet.applyTo(toolbarConstraintLayout);

        changeEditSearchConstraints();
    }

    private void changeEditSearchConstraints() {

        ConstraintSet constraintSet = new ConstraintSet();
        constraintSet.clone(toolbarConstraintLayout);
        constraintSet.connect(R.id.edit_text_search, ConstraintSet.START, R.id.image_view_search, ConstraintSet.END, 16);
        constraintSet.connect(R.id.edit_text_search, ConstraintSet.END, ConstraintSet.PARENT_ID, ConstraintSet.END, 16);
        constraintSet.applyTo(toolbarConstraintLayout);

        editSearch.setVisibility(View.VISIBLE);
        editSearch.requestFocus();
        clearIcon.setVisibility(View.VISIBLE);
        ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE)).toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
    }

    private void resetSearchIconConstraints() {
        editSearch.setVisibility(View.GONE);
        clearIcon.setVisibility(View.GONE);
        toolbarLogo.setVisibility(View.VISIBLE);

        ConstraintSet constraintSet = new ConstraintSet();
        constraintSet.clone(toolbarConstraintLayout);
        constraintSet.connect(R.id.image_view_search, ConstraintSet.END, ConstraintSet.PARENT_ID, ConstraintSet.END, 16);
        constraintSet.clear(R.id.image_view_search, ConstraintSet.START);
        constraintSet.applyTo(toolbarConstraintLayout);
    }

    private void startRotateAnimation(final Drawable drawable, float start, float end) {
        ObjectAnimator.ofFloat(clearIcon, "rotation", start, end).setDuration(400).start();
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                clearIcon.setImageDrawable(drawable);
            }
        }, 200);
    }

}
