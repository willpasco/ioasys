package com.android.ioasys.test.viewmodel;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.android.ioasys.test.repository.UserRepository;

import javax.inject.Inject;

public class UserViewModel extends ViewModel {

    private UserRepository userRepository;
    private MutableLiveData<Integer> requestCodeMutableLiveData;

    @Inject
    public UserViewModel(UserRepository userRepository) {
        this.userRepository = userRepository;
        requestCodeMutableLiveData = new MutableLiveData<>();
    }

    public void doLogin(String email, String password) {
        requestCodeMutableLiveData.setValue(null);
        requestCodeMutableLiveData = userRepository.doLogin(email, password);
    }

    public MutableLiveData<Integer> getRequestCodeMutableLiveData() {
        return requestCodeMutableLiveData;
    }
}
