package com.android.ioasys.test.service;

import javax.inject.Inject;

public class DataWrapper<T> {

    private T data;
    private int requestCode;

    @Inject
    public DataWrapper() {
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public int getRequestCode() {
        return requestCode;
    }

    public void setRequestCode(int requestCode) {
        this.requestCode = requestCode;
    }
}
