package com.android.ioasys.test.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class EnterpriseResponse {

    @SerializedName("enterprises")
    @Expose
    private List<Enterprise> enterpriseList = null;

    public List<Enterprise> getEnterpriseList() {
        return enterpriseList;
    }

    public void setEnterpriseList(List<Enterprise> enterpriseList) {
        this.enterpriseList = enterpriseList;
    }


}
