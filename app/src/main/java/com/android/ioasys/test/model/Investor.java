package com.android.ioasys.test.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Investor {

    private Integer id;
    @SerializedName("investor_name")
    @Expose
    private String investorName;
    private String email;
    private String city;
    private String country;
    private Integer balance;
    private String photo;
    private Portfolio portfolio;
    @SerializedName("portfolio_value")
    @Expose
    private Integer portfolioValue;
    @SerializedName("first_access")
    @Expose
    private Boolean firstAccess;
    @SerializedName("super_angel")
    @Expose
    private Boolean superAngel;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getInvestorName() {
        return investorName;
    }

    public void setInvestorName(String investorName) {
        this.investorName = investorName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Integer getBalance() {
        return balance;
    }

    public void setBalance(Integer balance) {
        this.balance = balance;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public Portfolio getPortfolio() {
        return portfolio;
    }

    public void setPortfolio(Portfolio portfolio) {
        this.portfolio = portfolio;
    }

    public Integer getPortfolioValue() {
        return portfolioValue;
    }

    public void setPortfolioValue(Integer portfolioValue) {
        this.portfolioValue = portfolioValue;
    }

    public Boolean getFirstAccess() {
        return firstAccess;
    }

    public void setFirstAccess(Boolean firstAccess) {
        this.firstAccess = firstAccess;
    }

    public Boolean getSuperAngel() {
        return superAngel;
    }

    public void setSuperAngel(Boolean superAngel) {
        this.superAngel = superAngel;
    }

}
