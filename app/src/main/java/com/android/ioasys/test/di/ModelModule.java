package com.android.ioasys.test.di;

import com.android.ioasys.test.model.AuthRequest;
import com.android.ioasys.test.model.Enterprise;
import com.android.ioasys.test.service.DataWrapper;

import java.util.List;

import dagger.Module;
import dagger.Provides;

@Module
public class ModelModule {

    @Provides
    public AuthRequest provideAuthRequest() {
        return new AuthRequest();
    }

    @Provides
    public DataWrapper<List<Enterprise>> provideEnterpriseListDataWrapper() {
        return new DataWrapper<>();
    }
}
