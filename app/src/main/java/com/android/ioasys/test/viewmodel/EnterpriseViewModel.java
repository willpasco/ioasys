package com.android.ioasys.test.viewmodel;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.android.ioasys.test.model.Enterprise;
import com.android.ioasys.test.repository.EnterpriseRepository;
import com.android.ioasys.test.service.DataWrapper;

import java.util.List;

import javax.inject.Inject;

public class EnterpriseViewModel extends ViewModel {
    private EnterpriseRepository repository;
    private MutableLiveData<DataWrapper<List<Enterprise>>> dataWrapperMutableLiveData;

    @Inject
    public EnterpriseViewModel(EnterpriseRepository repository) {
        this.repository = repository;
        this.dataWrapperMutableLiveData = new MutableLiveData<>();
    }

    public MutableLiveData<DataWrapper<List<Enterprise>>> getDataWrapperMutableLiveData() {
        return dataWrapperMutableLiveData;
    }

    public void fetchData(String enterpriseName) {
        repository.searchEnterprise(dataWrapperMutableLiveData, enterpriseName);
    }
}
