package com.android.ioasys.test.di;

import com.android.ioasys.test.android.adapter.EnterpriseRecyclerAdapter;

import dagger.Module;
import dagger.Provides;

@Module
public class ViewModule {

    @Provides
    public EnterpriseRecyclerAdapter provideEnterpriseAdapter() {
        return new EnterpriseRecyclerAdapter();
    }
}
