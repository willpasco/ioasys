package com.android.ioasys.test.android;

import android.annotation.SuppressLint;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.support.test.espresso.IdlingResource;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.ioasys.test.R;
import com.android.ioasys.test.di.ViewModelFactory;
import com.android.ioasys.test.service.IoasysService;
import com.android.ioasys.test.utils.SimpleIdlingResource;
import com.android.ioasys.test.viewmodel.UserViewModel;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.inject.Inject;

@SuppressLint("Registered")
@EActivity(R.layout.activity_login)
public class LoginActivity extends AppCompatActivity {

    private static final String TAG = "LoginActivity";
    @Inject
    ViewModelFactory viewModelFactory;

    @ViewById(R.id.edit_text_email)
    EditText editEmail;
    @ViewById(R.id.edit_text_password)
    EditText editPassword;
    @ViewById(R.id.button_login)
    Button buttonLogin;
    @ViewById(R.id.progress_bar_loading)
    ProgressBar loadingProgress;

    private UserViewModel userViewModel;
    private SimpleIdlingResource idlingResource;

    @AfterViews
    void afterViews() {
        getIdlingResource();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(getResources().getColor(R.color.alternativeStatusBar));
        }

        AppApplication.getComponent().inject(this);
        userViewModel = ViewModelProviders.of(this, viewModelFactory).get(UserViewModel.class);
    }

    public void doLogin(View view) {
        String email = editEmail.getText().toString();
        String password = editPassword.getText().toString();

        if (email.length() <= 0) {
            Toast.makeText(this, getResources().getString(R.string.email_required), Toast.LENGTH_LONG).show();
            return;
        }

        if (password.length() <= 0) {
            Toast.makeText(this, getResources().getString(R.string.password_require), Toast.LENGTH_LONG).show();
            return;
        }

        if (!isEmailValid(email)) {
            Toast.makeText(this, getResources().getString(R.string.email_invalid), Toast.LENGTH_LONG).show();
            return;
        }

        showLoadingState();
        idlingResource.setIdleState(false);
        userViewModel.doLogin(email, password);
        userViewModel.getRequestCodeMutableLiveData().observe(this, new Observer<Integer>() {
            @Override
            public void onChanged(@Nullable Integer code) {
                if (code != null) {
                    handleLoginRequestCode(code);
                }
            }
        });
    }

    private void handleLoginRequestCode(Integer code) {
        switch (code) {
            case IoasysService.REQUEST_OK:
                EnterpriseActivity_.intent(this).start();
                finish();
                break;
            case IoasysService.REQUEST_UNAUTHORIZED:
                showContent();
                Toast.makeText(LoginActivity.this, getString(R.string.login_unauthorized), Toast.LENGTH_LONG).show();
                break;
            default:
                showContent();
                Log.e(TAG, "Login code unexpected " + code);
                Toast.makeText(LoginActivity.this, getString(R.string.generic_error), Toast.LENGTH_LONG).show();
                break;
        }
        idlingResource.setIdleState(true);
    }

    private void showContent() {
        buttonLogin.setVisibility(View.VISIBLE);
        loadingProgress.setVisibility(View.GONE);
    }


    public void showLoadingState() {
        buttonLogin.setVisibility(View.GONE);
        loadingProgress.setVisibility(View.VISIBLE);
    }

    public static boolean isEmailValid(String email) {
        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    @VisibleForTesting
    @NonNull
    public IdlingResource getIdlingResource() {
        if (idlingResource == null) {
            idlingResource = new SimpleIdlingResource();
        }
        return idlingResource;
    }

}
