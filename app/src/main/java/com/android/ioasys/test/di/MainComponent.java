package com.android.ioasys.test.di;

import com.android.ioasys.test.android.EnterpriseActivity;
import com.android.ioasys.test.android.LoginActivity;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {
        ViewModelModule.class,
        RepositoryModule.class,
        NetworkModule.class,
        ViewModule.class})
public interface MainComponent {

    void inject(LoginActivity loginActivity);

    void inject(EnterpriseActivity enterpriseActivity);
}
