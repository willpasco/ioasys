package com.android.ioasys.test.di;

import com.android.ioasys.test.model.AuthRequest;
import com.android.ioasys.test.model.Enterprise;
import com.android.ioasys.test.repository.EnterpriseRepository;
import com.android.ioasys.test.repository.UserRepository;
import com.android.ioasys.test.service.DataWrapper;
import com.android.ioasys.test.service.IoasysService;

import java.util.List;

import dagger.Module;
import dagger.Provides;

@Module
public class RepositoryModule {

    @Provides
    public UserRepository provideUserRepository(IoasysService service, AuthRequest authRequest) {
        return new UserRepository(service, authRequest);
    }

    @Provides
    public EnterpriseRepository provideEnterpriseRepository(IoasysService service, DataWrapper<List<Enterprise>> dataWrapper) {
        return new EnterpriseRepository(service, dataWrapper);
    }

}
