package com.android.ioasys.test.service;

import com.android.ioasys.test.model.AuthRequest;
import com.android.ioasys.test.model.EnterpriseResponse;
import com.android.ioasys.test.model.UserResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface IoasysService {

    int REQUEST_OK = 200;
    int REQUEST_UNAUTHORIZED = 401;
    int REQUEST_APP_ERROR = -1;

    @POST("api/v1/users/auth/sign_in")
    @Headers("Content-Type: application/json")
    Call<UserResponse> doLogin(@Body AuthRequest authRequest);

    @GET("api/v1/enterprises")
    Call<EnterpriseResponse> searchEnterprise(@Query("name") String name,
                                              @Header("access-token") String accessToken,
                                              @Header("client") String client,
                                              @Header("uid") String uid);
}
