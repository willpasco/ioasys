package com.android.ioasys.test.model;

public class UserResponse {

    private Investor investor;
    private Object enterprise;
    private Boolean success;

    public Investor getInvestor() {
        return investor;
    }

    public void setInvestor(Investor investor) {
        this.investor = investor;
    }

    public Object getEnterprise() {
        return enterprise;
    }

    public void setEnterprise(Object enterprise) {
        this.enterprise = enterprise;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

}
