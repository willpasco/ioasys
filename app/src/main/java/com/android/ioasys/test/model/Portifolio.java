package com.android.ioasys.test.model;

import java.util.List;

class Portfolio {

    private Integer enterprisesNumber;
    private List<Object> enterprises = null;

    public Integer getEnterprisesNumber() {
        return enterprisesNumber;
    }

    public void setEnterprisesNumber(Integer enterprisesNumber) {
        this.enterprisesNumber = enterprisesNumber;
    }

    public List<Object> getEnterprises() {
        return enterprises;
    }

    public void setEnterprises(List<Object> enterprises) {
        this.enterprises = enterprises;
    }

}
