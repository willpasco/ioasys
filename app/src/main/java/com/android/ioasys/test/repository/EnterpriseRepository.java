package com.android.ioasys.test.repository;

import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;
import android.util.Log;

import com.android.ioasys.test.model.Enterprise;
import com.android.ioasys.test.model.EnterpriseResponse;
import com.android.ioasys.test.service.DataWrapper;
import com.android.ioasys.test.service.IoasysService;
import com.pixplicity.easyprefs.library.Prefs;

import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EnterpriseRepository {

    private static final String TAG = "EnterpriseRepository";
    private IoasysService service;
    private DataWrapper<List<Enterprise>> dataWrapper;

    @Inject
    public EnterpriseRepository(IoasysService service, DataWrapper<List<Enterprise>> dataWrapper) {
        this.service = service;
        this.dataWrapper = dataWrapper;
    }

    public void searchEnterprise(final MutableLiveData<DataWrapper<List<Enterprise>>> dataWrapperMutableLiveData, String enterpriseName) {
        String accessToken = Prefs.getString(UserRepository.ACCESS_TOKEN, null);
        String client = Prefs.getString(UserRepository.CLIENT, null);
        String uid = Prefs.getString(UserRepository.UID, null);

        Call<EnterpriseResponse> call = service.searchEnterprise(enterpriseName, accessToken, client, uid);

        call.enqueue(new Callback<EnterpriseResponse>() {
            @Override
            public void onResponse(@NonNull Call<EnterpriseResponse> call, @NonNull Response<EnterpriseResponse> response) {
                if (response.body() != null) {
                    dataWrapper.setData(response.body().getEnterpriseList());
                    dataWrapper.setRequestCode(response.code());
                } else {
                    Log.e(TAG, response.message());
                    dataWrapper.setRequestCode(response.code());
                }

                dataWrapperMutableLiveData.setValue(dataWrapper);
            }

            @Override
            public void onFailure(@NonNull Call<EnterpriseResponse> call, @NonNull Throwable t) {
                Log.e(TAG, t.getMessage());

                dataWrapper.setRequestCode(IoasysService.REQUEST_APP_ERROR);
                dataWrapperMutableLiveData.setValue(dataWrapper);
            }
        });
    }
}
