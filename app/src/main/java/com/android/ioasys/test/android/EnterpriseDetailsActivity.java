package com.android.ioasys.test.android;

import android.annotation.SuppressLint;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.ioasys.test.R;
import com.android.ioasys.test.utils.ImageLoader;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.ViewById;

@SuppressLint("Registered")
@EActivity(R.layout.activity_enterprise_details)
public class EnterpriseDetailsActivity extends AppCompatActivity {

    @ViewById
    Toolbar toolbar;

    @ViewById(R.id.image_view_enterprise_photo)
    ImageView imageViewEnterprisePhoto;

    @ViewById(R.id.text_view_enterprise_description)
    TextView textViewEnterpriseDescription;

    @ViewById(R.id.text_view_enterprise_initial)
    TextView textViewEnterpriseInitial;

    @ViewById(R.id.text_view_toolbar_title)
    TextView toolbarTitle;

    @Extra
    String enterpriseName;

    @Extra
    String enterprisePhotoUrl;

    @Extra
    String enterpriseDescription;

    @AfterViews
    void afterViews() {

        if (enterpriseName == null || enterpriseDescription == null) {
            Toast.makeText(this, "Desculpe! Não conseguimos carregar as informações!", Toast.LENGTH_LONG).show();
            finish();
        } else {
            toolbarTitle.setText(enterpriseName);

            textViewEnterpriseInitial.setText(String.valueOf(enterpriseName.charAt(0)));
            textViewEnterpriseDescription.setText(enterpriseDescription);

            if (enterprisePhotoUrl != null)
                ImageLoader.loadImageFromApiUrl(enterprisePhotoUrl, imageViewEnterprisePhoto, textViewEnterpriseInitial);
        }
    }

    public void backToPrevious(View view) {
        onBackPressed();
    }
}
