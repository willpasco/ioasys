![N|Solid](logo_ioasys.png)


## Getting Start
Apenas instalar e executar o app, fazendo login com o seguinte usuário de teste.

Email: testeapple@ioasys.com.br
Senha: 12341234


## Lib Explanation

Dagger 2 - Lib usada para realizar a injeção de dependência das classes.
Lifecyle - Lib usada para adicionar parte dos componentes do android architecture component (Live Data e View model)
Retrofit - Lib usada para o consumo de webservices.
Android Annotations - Lib usada para otimizar o onBind e outras questões relacionadas as views.
Easy Preferences - Lib utilizada para automatizar o processo de uso do Shared Preferences.
Glide - Lib que facilita o processo de load das imagens via Url externa e de resources internos.
Espresso - Lib utilizada para realizar testes instrumentados de UI.

## If i would had more time

* Implementaria mais testes de UI e unitários.
* Adicionaria algum campo visual para poder realizar pesquisas com filtro de tipos de empresa
* Adicionaria uma área para o usuário poder visualizar suas próprias informações.